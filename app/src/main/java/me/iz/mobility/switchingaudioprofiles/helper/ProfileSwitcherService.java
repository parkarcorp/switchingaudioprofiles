/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 1/22/16 1:05 PM
 *  @modified 1/22/16 1:05 PM
 */

package me.iz.mobility.switchingaudioprofiles.helper;

import android.app.IntentService;
import android.content.Intent;

import me.iz.mobility.switchingaudioprofiles.utils.AppConstants;
import me.iz.mobility.switchingaudioprofiles.utils.Preferences;
import timber.log.Timber;

public class ProfileSwitcherService extends IntentService {

    public ProfileSwitcherService() {
        super("ProfileSwitcherService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            Preferences.init(getApplicationContext());

            boolean wifiConnected = intent.getBooleanExtra(AppConstants.WIFI_CONNECTED.name(), false);

            Timber.i("Wifi status "+wifiConnected);


            if(wifiConnected) {
                String selectedProfile = Preferences.getString(AppConstants.SELECTED_PROFILE.name());

                String currentProfile = ProfileSwitcher.getInstance().getCurrentProfileMode();
                Timber.d("Current profile is "+currentProfile);
                Preferences.saveString(AppConstants.CURRENT_PROFILE.name(), currentProfile);
                Timber.d("Switching to "+selectedProfile);
                ProfileSwitcher.getInstance().switchToProfile(selectedProfile);
                return;
            }

            String prevProfile = Preferences.getString(AppConstants.CURRENT_PROFILE.name());
            Timber.d("Switching back to "+prevProfile);
            if(prevProfile.isEmpty()) {
                Timber.d("No Prev profile found.");
                return;
            }

            ProfileSwitcher.getInstance().switchToProfile(prevProfile);






        }
    }
}
