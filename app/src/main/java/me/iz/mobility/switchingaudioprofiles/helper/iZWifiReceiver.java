/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 1/22/16 12:11 PM
 *  @modified 1/22/16 12:11 PM
 */

package me.iz.mobility.switchingaudioprofiles.helper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

import me.iz.mobility.switchingaudioprofiles.utils.AppConstants;
import timber.log.Timber;

public class iZWifiReceiver extends BroadcastReceiver {

//    private static final String wifiName = "WiFly-EZX-e3";

    private static final String wifiName = "MY_ZONE";


    public iZWifiReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {

        boolean connected = false;

        Timber.d("Wifi Broadcast received ");
        String action = intent.getAction();
        if (WifiManager.SUPPLICANT_STATE_CHANGED_ACTION .equals(action)) {
            SupplicantState state = intent.getParcelableExtra(WifiManager.EXTRA_NEW_STATE);
            if (SupplicantState.isValidState(state)
                    && state == SupplicantState.COMPLETED) {

                connected = checkConnectedToDesiredWifi(context);
            }
        }

        Intent profileIntent = new Intent(context, ProfileSwitcherService.class);
        profileIntent.putExtra(AppConstants.WIFI_CONNECTED.name(),connected);
        context.startService(profileIntent);
    }

    /** Detect you are connected to a specific network. */
    private boolean checkConnectedToDesiredWifi(Context context) {
        boolean connected = false;

        String desiredMacAddress = wifiName;

        WifiManager wifiManager =
                (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        WifiInfo wifi = wifiManager.getConnectionInfo();
        if (wifi != null) {
            // get current router Mac address
            String bssid = wifi.getSSID().replaceAll("\"","");
            connected = desiredMacAddress.equals(bssid);
        }

        return connected;
    }


}
