/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 1/18/16 5:58 PM
 *  @modified 1/18/16 5:58 PM
 */

package me.iz.mobility.switchingaudioprofiles;

import android.app.Application;
import android.content.Context;

import me.iz.mobility.switchingaudioprofiles.utils.Preferences;
import timber.log.Timber;

/**
 * @author ibasit
 */
public class SwitchingAudioProfiles extends Application {

    private final String TAG = getClass().getSimpleName();

    private static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();

        Preferences.init(this);

        mContext = this;

        if(BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }

    public static Context getAppContext() {
        return mContext;
    }
}
