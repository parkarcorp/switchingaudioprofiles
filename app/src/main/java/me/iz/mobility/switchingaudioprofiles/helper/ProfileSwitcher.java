/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 1/22/16 12:51 PM
 *  @modified 1/22/16 12:51 PM
 */

package me.iz.mobility.switchingaudioprofiles.helper;

import android.content.Context;
import android.media.AudioManager;

import me.iz.mobility.switchingaudioprofiles.R;
import me.iz.mobility.switchingaudioprofiles.SwitchingAudioProfiles;

/**
 * @author ibasit
 */
public class ProfileSwitcher {

    private static ProfileSwitcher mInstance;

    private AudioManager profileMode;

    public static ProfileSwitcher getInstance() {

        if(mInstance == null)
            mInstance = new ProfileSwitcher();

        return mInstance;
    }

    public ProfileSwitcher() {
        profileMode = (AudioManager) SwitchingAudioProfiles.getAppContext().
                getSystemService(Context.AUDIO_SERVICE);
    }

    public String getCurrentProfileMode() {

        int currentProfile = profileMode.getRingerMode();

        if(currentProfile == AudioManager.RINGER_MODE_SILENT)
            return SwitchingAudioProfiles.getAppContext().
                getString(R.string.silent_mode);

        if (currentProfile == AudioManager.RINGER_MODE_NORMAL)
            return SwitchingAudioProfiles.getAppContext().
                    getString(R.string.loud_mode);

        return SwitchingAudioProfiles.getAppContext().getString(R.string.vibrate_mode);
    }

    public void switchToProfile(String profileName) {

        if (profileName.equalsIgnoreCase(SwitchingAudioProfiles.getAppContext().getString(R.string.silent_mode))) {
            profileMode.setRingerMode(AudioManager.RINGER_MODE_SILENT);
        } else if (profileName.equalsIgnoreCase(SwitchingAudioProfiles.getAppContext().getString(R.string.loud_mode))) {
            profileMode.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        } else {
            profileMode.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
        }
    }
}
