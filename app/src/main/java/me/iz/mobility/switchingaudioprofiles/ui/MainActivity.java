/*
 * Copyright 2016 CarIQ Technologies Pvt. Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 *  @author Basit Parkar
 *  @date 1/18/16 6:03 PM
 *  @modified 1/18/16 5:48 PM
 */

package me.iz.mobility.switchingaudioprofiles.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import butterknife.Bind;
import butterknife.OnClick;
import me.iz.mobility.switchingaudioprofiles.R;
import me.iz.mobility.switchingaudioprofiles.helper.ProfileSwitcher;
import me.iz.mobility.switchingaudioprofiles.utils.AppConstants;
import me.iz.mobility.switchingaudioprofiles.utils.BaseActivity;
import me.iz.mobility.switchingaudioprofiles.utils.CustomMessage;
import me.iz.mobility.switchingaudioprofiles.utils.Preferences;

public class MainActivity extends BaseActivity {

    @Bind(R.id.spProfile)
    Spinner spProfile;

    private String selectedProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


//        spProfile = (Spinner) findViewById(R.id.spProfile);


        setupProfileSpinner();

    }

    private void setupProfileSpinner() {

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>
                (mContext, R.layout.list_item_spinner, getResources().
                        getStringArray(R.array.profiles));
        dataAdapter.setDropDownViewResource
                (android.R.layout.simple_spinner_dropdown_item);
        spProfile.setAdapter(dataAdapter);

        spProfile.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedProfile = (String) spProfile.getItemAtPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        selectedProfile = Preferences.getString(AppConstants.SELECTED_PROFILE.name());

        if (selectedProfile.isEmpty()) {
            selectedProfile = (String) spProfile.getItemAtPosition(0);
            return;
        }
        int pos = getProfilePosition(dataAdapter, selectedProfile);
        spProfile.setSelection(pos);

    }

    /**
     * Get Position in spinner for particular Make id
     */
    public int getProfilePosition(ArrayAdapter<String> profileList, String currentProfile) {

        if (profileList.isEmpty()) {
            return 0;
        }

        int pos = 0;
        for (; pos < profileList.getCount(); pos++) {
            if (profileList.getItem(pos).equals(currentProfile)) {
                break;
            }
        }

        return pos;
    }


    @OnClick(R.id.btnSave)
    public void saveProfile() {

        Preferences.saveString(AppConstants.SELECTED_PROFILE.name(), selectedProfile);
        CustomMessage.showSnackBar(spProfile, selectedProfile + " mode configured.");
    }

    @OnClick(R.id.btnSet)
    public void setProfile() {

        ProfileSwitcher.getInstance().switchToProfile(selectedProfile);
        CustomMessage.showSnackBar(spProfile, selectedProfile + " mode activated.");
    }


    @OnClick(R.id.btnAboutMe)
    public void aboutUs() {

        Intent intent = new Intent(this,AboutActivity.class);
        startActivity(intent);
    }


}
